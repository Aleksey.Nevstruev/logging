﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logging.AlgorithmImplementation
{
    /// <summary>
    /// class to calculate div for two numbers.
    /// </summary>
    public class DivCalculator : IAlgorithm
    {
        /// <summary>
        /// calculate div for two numbers.
        /// </summary>
        /// <param name="first">first number</param>
        /// <param name="second">second number</param>
        /// <returns>div for two numbers.</returns>
        public int Calculate(int first, int second) => first / second;
    }
}
