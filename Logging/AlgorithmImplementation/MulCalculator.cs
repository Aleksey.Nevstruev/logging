﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logging
{
    /// <summary>
    /// class to calculate mul for two numbers.
    /// </summary>
    public class MulCalculator : IAlgorithm
    {
        /// <summary>
        /// calculate mul for two numbers.
        /// </summary>
        /// <param name="first">first number</param>
        /// <param name="second">second number</param>
        /// <returns>mul for two numbers.</returns>
        public int Calculate(int first, int second) => first * second;
    }
}
