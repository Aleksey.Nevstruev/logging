﻿namespace Logging
{
    /// <summary>
    /// class to calculate sum for two numbers.
    /// </summary>
    public class SumCalculator : IAlgorithm
    {
        /// <summary>
        /// calculate sum for two numbers.
        /// </summary>
        /// <param name="first">first number</param>
        /// <param name="second">second number</param>
        /// <returns>sum for two numbers.</returns>
        public int Calculate(int first, int second) => first + second;
    }
}
