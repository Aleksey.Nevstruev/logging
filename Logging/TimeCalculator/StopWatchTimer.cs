﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;

namespace Logging.TimeCalculatorFactory
{
    public class StopWatchTimer : ITimer
    {
        public readonly Stopwatch _stopwatch;

        public StopWatchTimer()
        {
            this._stopwatch = new Stopwatch();
        }

        public void Start()
        {
            this._stopwatch.Start();
        }

        public long Stop()
        {
            this._stopwatch.Stop();
            return this._stopwatch.ElapsedMilliseconds;
        }
    }
}
