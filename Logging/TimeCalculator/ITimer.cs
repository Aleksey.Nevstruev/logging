﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Logging.TimeCalculatorFactory
{
    public interface ITimer
    {
        void Start();

        long Stop();
    }
}
