﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.Extensions.Logging;
using Microsoft.VisualBasic;

namespace Logging
{

    /// <summary>
    /// class to logging.
    /// </summary>
    public class Logger : IAlgorithm
    {
        private readonly ILogger logger;
        private readonly IAlgorithm algorithm;

        /// <summary>
        /// Initialize instance of class Logger.
        /// </summary>
        /// <param name="algorithm"> Instance performs operation.</param>
        /// <param name="loggerFactory">factory to create logger object.</param>
        public Logger(IAlgorithm algorithm, ILoggerFactory loggerFactory)
        {
            this.logger = loggerFactory.CreateLogger(nameof(Logger));
            this.algorithm = algorithm;
        }

        /// <summary>
        /// Perform logging while executing calculate method by algorithm.
        /// </summary>
        /// <param name="first">first operand.</param>
        /// <param name="second">second operand.</param>
        /// <returns>result of operation.</returns>
        public int Calculate(int first, int second)
        {
            try
            {
                return this.algorithm.Calculate(first, second);
            }
            catch (Exception e)
            {
                this.logger.LogError(e, "error while executing calculate method");
                throw;
            }
        }
    }
}
