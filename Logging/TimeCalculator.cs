﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using Logging.TimeCalculatorFactory;

namespace Logging
{
    /// <summary>
    /// class to calculate elapsed time .
    /// </summary>
    public class TimeCalculator : IAlgorithm
    {
        private readonly IAlgorithm algorithm;

        private readonly ITimer timer;

        /// <summary>
        /// gets millisecond.
        /// </summary>
        public long Milliseconds { get; private set; }

        /// <summary>
        /// Initialize instance of class TimeCalculator.
        /// </summary>
        /// <param name="algorithm"> Instance performs operation.</param>
        public TimeCalculator(IAlgorithm algorithm, ITimer timer)
        {
            this.algorithm = algorithm;
            this.timer = timer;
        }

        /// <summary>
        /// Calculate elapsed time while executing calculate method.
        /// </summary>
        /// <param name="first">first operand.</param>
        /// <param name="second">second operand.</param>
        /// <returns>result of operation.</returns>
        public int Calculate(int first, int second)
        {
           this.timer.Start();
            var result = this.algorithm.Calculate(first, second);
            this.Milliseconds = this.timer.Stop();
            return result;
        }
    }
}
