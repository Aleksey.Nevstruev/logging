﻿using System;
using Logging.AlgorithmImplementation;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Logging.Console;
using NUnit;
using NUnit.Framework;
using NUnit.Framework.Internal;


namespace Logging.Tests
{
    [TestFixture]
    public class LoggerTests
    {
        [Test]
        public void CalculateDiv_DivByZeroJsonConsoleProvider_ThrowException()
        {
            var logFactory = LoggerFactory.Create(
                logBuilder => logBuilder.
                    AddJsonConsole()
            );
            var logger = new Logger(new DivCalculator(), logFactory);
            Assert.Throws<DivideByZeroException>(() => logger.Calculate(1, 0));
        }

        [Test]
        public void CalculateSum_DivByZeroConsoleProvider_ReturnResult()
        {
            var logFactory = LoggerFactory.Create(
                logBuilder => logBuilder.
                    AddConsole()
            );
            var logger = new Logger(new DivCalculator(), logFactory);
            Assert.Throws<DivideByZeroException>(() => logger.Calculate(int.MaxValue, 0));
        }
    }
}
