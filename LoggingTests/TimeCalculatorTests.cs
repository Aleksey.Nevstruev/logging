using Logging.TimeCalculatorFactory;
using NUnit.Framework;

namespace Logging.Tests
{
    public class TimeCalculatorTests
    {
        [Test]
        public void CalculateSum_PositiveNumbers_ReturnResult()
        {
            var algorithm = new TimeCalculator(new SumCalculator(), new StopWatchTimer());

            var result = algorithm.Calculate(1, 2);
            var expected = 3;

            Assert.That(result, Is.EqualTo(expected));
        }

        [Test]
        public void CalculateMul_PositiveNumbers_ReturnResult()
        {
            var algorithm = new TimeCalculator(new MulCalculator(), new StopWatchTimer());

            var result = algorithm.Calculate(1, 2);
            var expected = 2;

            Assert.That(result, Is.EqualTo(expected));
        }
    }
}